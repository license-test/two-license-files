# Used third party content

## CI to build a Docker image

-   `.gitlab-ci.yml` file copied and modified from the GitLab Documentation (as of 2021-01-10, the contents of the script seem to have changed by now)
    -   Title of the documentation page: "Building a Docker image with kaniko"
    -   Author: (c) 2011-present GitLab B.V.
    -   URL: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko
    -   License: CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)

## Traefik configuration

-   Parts of the Traefik configuration are adapted from the Traefik documentation
    -   Author: (c) 2016-2020 Containous SAS; 2020-2023 Traefik Labs
    -   URL: https://doc.traefik.io/traefik/, GitHub: https://github.com/traefik/traefik/tree/master/docs
    -   License: [The MIT License (MIT)](https://github.com/traefik/traefik/blob/master/LICENSE.md)

The adapted parts are marked specially in the respective files.

## flaskr (Flask Tutorial App)

Some files in this project are more or less modified versions of the files from the Flask Tutorial App `flaskr`, which
is available at:

-   https://github.com/pallets/flask/tree/2.0.3/examples/tutorial
-   https://flask.palletsprojects.com/en/2.0.x/tutorial/
-   https://github.com/pallets/flask/tree/2.2.x/examples/tutorial
-   https://flask.palletsprojects.com/en/2.2.x/tutorial/tests/

It is licensed under the following license (see also https://flask.palletsprojects.com/en/2.0.x/license/):

### BSD-3-Clause License

Copyright 2010 Pallets

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1.  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Flask Documentation

Some snippets in this project are adapted from the Flask Documentation, which is available at:

-   https://github.com/pallets/flask/tree/2.1.x/docs
-   https://flask.palletsprojects.com/en/2.1.x/

The documentation is licensed under the license mentioned above (see also https://flask.palletsprojects.com/en/2.1.x/license/).

## Flask SQLAlchemy Documentation

Some snippets in this project are adapted from the Flask SQLAlchemy Documentation, which is available at:

-   https://flask-sqlalchemy.palletsprojects.com/en/2.x/

The documentation is licensed under BSD-3-Clause License (https://github.com/pallets-eco/flask-sqlalchemy/blob/main/docs/license.rst),
we assume this is a reference to the following license document (https://flask-sqlalchemy.palletsprojects.com/en/2.x/license/):

### BSD-3-Clause License

Copyright 2010 Pallets

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1.  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Material Icons

-   Files: `schwalbe/static/lib/material-icons-*`
    -   URL: https://github.com/google/material-design-icons
    -   Author: Google
    -   License: [Apache License 2.0](https://github.com/google/material-design-icons/blob/master/LICENSE)
-   File: `schwalbe/static/lib/material-icons-*/css/material-icons.css`
    -   Derived (with modifications) from URL: https://developers.google.com/fonts/docs/material_icons?hl=en#setup_method_2_self_hosting
    -   Author: Google
    -   License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
-   File: `schwalbe/static/img/favicon_poll_green.svg`
    -   Derived (with modifications) from icon "poll", URL: https://fonts.google.com/icons?selected=Material%20Icons%3Apoll%3A
    -   Author: Google
    -   License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Material Textfields

-   Files: `schwalbe/static/lib/material-textfields/*`
    -   Derived (with modifications) from URL: https://codepen.io/co0kie/pen/KKKEeWv
    -   Author: co0kie
    -   License: [MIT License](schwalbe/static/lib/material-textfields/LICENSE)

## "Polygon Luminary" Background

The background used on the login page.

-   File: `schwalbe/static/img/polygon_luminary.svg`
    -   Generated by: https://bgjar.com/polygon-luminary
    -   Author: BGJar.com
    -   License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

## Redoc

-   Files: `schwalbe/static/lib/redoc/*`
    -   Author: Rebilly, Inc.
    -   URL: https://github.com/Redocly/redoc
    -   License: [MIT License](schwalbe/static/lib/redoc/LICENSE)

## Mithril.js

-   Files: `schwalbe/static/lib/mithril/*`
    -   Author: Leo Horie
    -   URL: https://github.com/MithrilJS/mithril.js
    -   License: [MIT License](schwalbe/static/lib/mithril/LICENSE)

## base64.js

-   Files: `schwalbe/static/lib/base64-*`
    -   Author: Dan Kogai
    -   URL: https://github.com/dankogai/js-base64
    -   License: [BSD 3-Clause License](schwalbe/static/lib/base64-3.7.5/LICENSE.md)

# Resources consulted

Here, we list a few notable resources we consulted during development. None of this content is incorporated directly
into the application, however, so we decided to list it in a separate section of this document.

## Material Theme

Although we do not embed the library directly, our UI is inspired by the Material Theme components of this reference implementation:

-   Material Components for the web
    -   URL: https://github.com/material-components/material-components-web
    -   Author: (c) 2014-2020 Google, Inc.
    -   License: The MIT License

### The MIT License

The MIT License

Copyright (c) 2014-2020 Google, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
